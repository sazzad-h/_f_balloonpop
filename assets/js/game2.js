let memoryCards = [
{
  id: 1,
  frontImage: "assets/images/aurelia.svg",
  frontAlt: "Aurelia",
  order: 1,
  status: "Confirmed"
},
{
  id: 2,
  frontImage: "assets/images/vue.svg",
  frontAlt: "Vue",
  order: 2,
  status: "Confirmed"
},
{
  id: 3,
  frontImage: "assets/images/angular.svg",
  frontAlt: "Angular",
  order: 3,
  status: "Confirmed"
},
{
  id: 4,
  frontImage: "assets/images/ember.svg",
  frontAlt: "Ember",
  order: 4,
  status: "Confirmed"
},
{
  id: 5,
  frontImage: "assets/images/backbone.svg",
  frontAlt: "Backbone",
  order: 5,
  status: "Confirmed"
},
{
  id: 6,
  frontImage: "assets/images/react.svg",
  frontAlt: "React",
  order: 6,
  status: "Confirmed"
}
];

class SelectScene extends Phaser.Scene {
  constructor (){
    super('SelectScene');
  }

  preload (){  
    for (var i = 1; i < 28; i++) {
      this.load.image('balloon'+i, 'assets/images/Balloon'+i+'.png');;
    }
    this.load.image('background', 'assets/images/Background.png');
    this.load.image('CloseButton', 'assets/images/CloseButton.png');
    this.load.image('HideButton', 'assets/images/HideButton.png');
    this.load.image('NextButton', 'assets/images/NextButton.png');
    this.load.image('PickButton', 'assets/images/PickButton.png');
    this.load.image('ShowButton', 'assets/images/ShowButton.png');
    this.load.image('PickDialog', 'assets/images/PickDialog.png');
    this.load.image('String', 'assets/images/String.png');
    this.load.image('Easel', 'assets/images/Easel.png');
  }

  create (){
    var gi = new GameInfo({
      name: "Balloon Pop",
      width: window.innerWidth,
      height: window.innerHeight,
      autoScale: true,
      themes: ["default"],
      gamesetsAllowed: true,  
      minimumGamesetCardsAllowed: 6,
      isTurnTaking: false,
      allowGameCardNavigation: true,
    });
    sendToGameshell({
      eventType: "gameReady",
      message: gi
    });

    this.background = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'background').setDisplaySize(window.innerWidth,window.innerHeight);
    var scale = this.background.scale;
    var scaleX = this.background.scaleX;
    var scaleY = this.background.scaleY;

    this.PickDialog = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'PickDialog').setScale(scaleX,scaleY);
    var closePos = this.PickDialog.getTopRight();
    this.CloseButton = this.add.image(closePos.x-20,closePos.y+20, 'CloseButton').setScale(scale*.9)
    .setOrigin(0.5, 0.5).setInteractive()
    .on('pointerdown', function () {
      console.log("[Get Chips]");
      this.scene.start('GameScene', {balloonNo:1});
    }.bind(this))
    .on('pointerover', function (pointer) { this.CloseButton.setScale(scale*1.1); }.bind(this))
    .on('pointerout', function (pointer) { this.CloseButton.setScale(scale*.9); }.bind(this));
    var width = window.innerWidth /6;
    var height = window.innerHeight /5;
    var i=1;
    for (var row = 0; row < 5; row++) {
      for(var col=0;col<4;col++){
        this.add.sprite(width + row*width,height+ col*height, 'balloon'+i).setOrigin(.5).setScale(scaleX*.35,scaleY*.35).setInteractive()
        .setData('balloon', i); 
        i++;
      }
    }
    this.input.on('gameobjectover', function (pointer, gameObject) {
      var balloonNo = gameObject.getData('balloon');
      if(balloonNo!=null){
        gameObject.setScale(scaleX*.4,scaleY*.4);
      }
    }, this);
    this.input.on('gameobjectout', function (pointer, gameObject) {
      var balloonNo = gameObject.getData('balloon');
      if(balloonNo!=null){
        gameObject.setScale(scaleX*.35,scaleY*.35);
      }
    }, this);

    this.input.on('gameobjectdown', function (pointer, gameObject) {
      var balloonNo = gameObject.getData('balloon');
      if(balloonNo!=null){
        // console.log("Selected Balloon="+balloonNo);
        this.scene.start('GameScene', {balloonNo:balloonNo});

      }
    }, this);

console.log("before emit");
  game.events.emit('customfn');
  this.customfn();


  } // end of create 

  customfn(){
    console.log("this is customfn from scene");
  }

} // end of scene

class GameScene extends Phaser.Scene {

  constructor (){
    super('GameScene');
  }

  init(data){
    // console.log("Selected Balloon on init="+data.balloonNo);
    this.balloonNo = data.balloonNo;
  }


  preload (){  
    for (var i = 0; i < memoryCards.length; i++) {
      this.load.image('card'+memoryCards[i].id, memoryCards[i].frontImage);;
    }
  }

  create (){
    // console.log("Selected Balloon="+data.balloonNo);

    this.background = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'background').setDisplaySize(window.innerWidth,window.innerHeight);
    var scale = this.background.scale;
    var scaleX = this.background.scaleX;
    var scaleY = this.background.scaleY;

    this.Easel = this.add.image(window.innerWidth * .2, window.innerHeight * .6, 'Easel').setScale(scale*1);
    this.card = this.add.image(this.Easel.x, this.Easel.y - 80, 'card'+memoryCards[0].id).setScale(scale*.8);
    this.cardTitle   = this.add.text(this.Easel.x, this.Easel.y + 20, memoryCards[0].frontAlt, { 
      font: "38px",
      wordWrap: { width: 520, useAdvancedWrap: true },
      color: '#33121d',
      align: 'center'
    }).setOrigin(.5).setAlpha(0);

    this.String = this.add.image(window.innerWidth * .65, window.innerHeight * .55 -5, 'String')
    .setOrigin(.5,0).setScale(scale*.7); 
    this.balloon = this.add.image(window.innerWidth * .65, window.innerHeight * .55, 'balloon'+this.balloonNo)
    .setOrigin(.5,1).setScale(scale*.7);
    
    this.balloonMove = this.tweens.add({
      targets: [this.balloon,this.String],
      ease: 'Sine.easeInOut',
      duration: 3000,
      x:"+=100",
      y: { value: "+=50", duration: 1500, ease: 'Bounce.easeOut',yoyo:true },
      yoyo:true,
      repeat:-1
    });

    this.clickCount = 0;
    this.currentWord = 0;

    this.balloon.setInteractive({ cursor: 'url(assets/pointer2.cur), pointer' })
    .on('pointerdown', function () {
      // this.pumpBalloon();

      this.clickCount++;
      if(this.clickCount<5){
        this.balloon.scale += .05;
      }else{
        this.clickCount=0;
        this.nextWord(true);
      }

    }.bind(this))
    .on('pointerover', function (pointer) { this.balloonMove.pause(); }.bind(this))
    .on('pointerout', function (pointer) { this.balloonMove.resume(); }.bind(this));

    this.NextButton = this.add.image(window.innerWidth *.2, window.innerHeight * .9, "NextButton").setScale(scale*.9)
    .setOrigin(0.5, 0.5).setInteractive()
    .on('pointerdown', function () {

      this.clickCount++;
      if(this.clickCount<5){
        this.balloon.scale += .05;
        this.nextWord(false);
      }else{
        this.clickCount=0;
        this.nextWord(true);
      }

    }.bind(this))
    .on('pointerover', function (pointer) { this.NextButton.setScale(scale*1.1); }.bind(this))
    .on('pointerout', function (pointer) { this.NextButton.setScale(scale*.9); }.bind(this));

    this.isShown = true;

    this.ShowButton = this.add.image(window.innerWidth *.5, window.innerHeight * .9, "ShowButton").setScale(scale*.9)
    .setOrigin(0.5, 0.5).setInteractive()
    .on('pointerdown', function () {
      if(this.isShown) {
        this.ShowButton.setTexture("HideButton"); 
        this.cardTitle.setAlpha(1);
        this.isShown = false;
      }
      else{
        this.cardTitle.setAlpha(0);
        this.ShowButton.setTexture("ShowButton"); 
        this.isShown = true;
      }
    }.bind(this))
    .on('pointerover', function (pointer) { this.ShowButton.setScale(scale*1.1); }.bind(this))
    .on('pointerout', function (pointer) { this.ShowButton.setScale(scale*.9); }.bind(this));

    this.PickButton = this.add.image(window.innerWidth *.8, window.innerHeight * .9, "PickButton").setScale(scale*.9)
    .setOrigin(0.5, 0.5).setInteractive()
    .on('pointerdown', function () {
      this.scene.start('SelectScene');
    }.bind(this))
    .on('pointerover', function (pointer) { this.PickButton.setScale(scale*1.1); }.bind(this))
    .on('pointerout', function (pointer) { this.PickButton.setScale(scale*.9); }.bind(this));

    this.scale = scale;
  } // end of create

  pumpBalloon(){
    this.clickCount++;
    if(this.clickCount<5){
      this.balloon.scale += .05;
    }else{
      this.clickCount=0;
      this.nextWord(true);
    }
  }

  nextWord(withAnim){
    this.currentWord++;
    if(this.currentWord>=6)this.currentWord=0;
    this.card.setTexture('card'+memoryCards[this.currentWord].id);
    this.cardTitle.setText(memoryCards[this.currentWord].frontAlt);
    if(withAnim){
      this.NextButton.disableInteractive();
      this.tweens.add({
        targets: [this.balloon,this.String],
        ease: 'Sine.easeInOut',
        duration: 300,
        alpha:0,
        onCompleteParams: [this.balloon,this.String],
        onCompleteScope: this,
        onComplete: function(){
          this.balloon.setAlpha(1);
          this.balloon.setScale(this.scale*.7);
          this.String.setAlpha(1);
          this.balloon.y += 1000;
          this.String.y += 1000;
        }
      });
      this.tweens.add({
        targets: [this.balloon,this.String],
        ease: 'Sine.easeInOut',
        delay: 300,
        duration: 3000,
        y: "-=1000",
        onCompleteParams: [this.NextButton],
        onCompleteScope: this,
        onComplete: function(){
          this.NextButton.setInteractive();

        }
      });
    }
    
  }


}



var config = {
  type: Phaser.AUTO,
  parent: 'phaser-game',
  width: window.innerWidth,
  height: window.innerHeight,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  scene: [ SelectScene, GameScene ],
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
      gravity: { y: 0 }
    }
  },
};
const game = new Phaser.Game(config);

function startGameHook() {
  console.log("Start game call")

  // send message to all other games to start their game
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "startGame",
      data: {  }
    }
  });
}

  function customfn(){
    console.log("this is customfn outside of the scene");
  }
 function bindEvent2(element, eventName, eventHandler) {
  if (element.addEventListener) {
    element.addEventListener(eventName, eventHandler, false);
  } else if (element.attachEvent) {
    element.attachEvent("on" + eventName, eventHandler);
  }
}

