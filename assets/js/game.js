let memoryCards = [
{id: 1,frontImage: "assets/images/aurelia.svg",frontAlt: "Aurelia",order: 1,status: "Confirmed"},
{id: 2,frontImage: "assets/images/vue.svg",frontAlt: "Vue",order: 2,status: "Confirmed"},
{id: 3,frontImage: "assets/images/angular.svg",frontAlt: "Angular",order: 3,status: "Confirmed"},
{id: 4,frontImage: "assets/images/ember.svg",frontAlt: "Ember",order: 4,status: "Confirmed"},
{id: 5,frontImage: "assets/images/backbone.svg",frontAlt: "Backbone",order: 5,status: "Confirmed"},
{id: 6,frontImage: "assets/images/react.svg",frontAlt: "React",order: 6,status: "Confirmed"}
];
let themes = {
  default: {
    backImage: "./assets/js-badge.svg",
    backAlt: "JS Badge",
    cardBackgroundColor: "lightgreen",
    gameBackgroundColor: "darkolivegreen",
    playerNamesColor: "#000000"
  },
  tinyeye: {
    backImage: "./assets/te-butterfly.png",
    backAlt: "TE Butterfly",
    cardBackgroundColor: "#1C7CCC",
    gameBackgroundColor: "#060AB2",
    playerNamesColor: "#FFFFFF"
  }
};

let currentMemoryCards = memoryCards.slice(0);
let isCustomGameSet = false;
let maxNumberOfUniqueCards = 6;
let cardsOrder = [];

// default variables
let currentThemeName = "default";
let currentTheme = themes["default"];
let lockBoard = false;
let firstCard, secondCard;
let matchesFound = 0;
// 1 is student userType
// 2 = therapist
// let userType = 1;
let userType;

let players = {};
let playerScores = {};
let localPlayerIds = [];
let currentPlayer = null;

let isGameReady = false;
let messageQueue = [];


var self = null;

// ================================================================
var BootScene = new Phaser.Scene('BootScene');
BootScene.preload = function () {
  this.load.image("logo", "assets/logo.png");
  self = this;
};

BootScene.create = function () {
  this.scene.start('Preloader');
};


// ================================================================
var Preloader = new Phaser.Scene('Preloader');
Preloader.preload = function () {
  self = this;

  var width = config.width;
  var height = config.height;
  var bar_start = config.width/5;
  var bar_end = bar_start * 4;
  var bar_size = bar_end - bar_start;

  console.log("Preloader.width"+width);
  console.log("Preloader.height"+height);

  var progressBar = this.add.graphics().setDepth(10);
  var progressBox = this.add.graphics();
  progressBox.fillStyle(0x222222, 0.8);
  progressBox.fillRect(bar_start, config.height/2 + 130, bar_size, 15);

  var background = this.add.sprite(config.width/2, config.height/2, 'logo').setScale(.5);

  this.load.on('progress', function (value) {
    progressBar.clear();
    progressBar.fillStyle(0xffffff, 1);
    progressBar.fillRect(bar_start + 5, config.height/2 + 135, bar_size * value, 10);
  });

  console.log("User type="+userType);
  this.StartBtn   = this.add.text(500, config.height *.8, "Start Game", { 
    font: "38px",
    wordWrap: { width: 520, useAdvancedWrap: true },
    color: '#46B5F4',
    align: 'center'
  }).setScale(.9).setAlpha(0).setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    this.startGame();
  }.bind(this))
  .on('pointerover', function (pointer) { this.StartBtn.setScale(1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.StartBtn.setScale(.9); }.bind(this));
  

  this.load.on('complete', function () {
    progressBar.destroy();
    progressBox.destroy();
    if(userType==null || userType==2){
      this.StartBtn.setAlpha(1);
    }
  }.bind(this));

  for (var i = 1; i < 28; i++) {
    this.load.image('balloon'+i, 'assets/images/Balloon'+i+'.png');;
  }
  this.load.image('background', 'assets/images/Background.png');
  this.load.image('CloseButton', 'assets/images/CloseButton.png');
  this.load.image('HideButton', 'assets/images/HideButton.png');
  this.load.image('NextButton', 'assets/images/NextButton.png');
  this.load.image('PickButton', 'assets/images/PickButton.png');
  this.load.image('ShowButton', 'assets/images/ShowButton.png');
  this.load.image('PickDialog', 'assets/images/PickDialog.png');
  this.load.image('String', 'assets/images/String.png');
  this.load.image('Easel', 'assets/images/Easel.png');

};

Preloader.startGame = function(data){
  this.scene.start('SelectScene');


  
  if (data!=null && data.startingPlayerId!=null) {
    currentPlayer = players[data.startingPlayerId];
  }
}
Preloader.endGame = function(){
  this.scene.start('EndGameScene');
}

Preloader.create = function () {
  //this.scene.start('SelectScene');

  console.log("Preloader.GameInfo.width"+window.innerWidth);
  console.log("Preloader.GameInfo.height"+window.innerHeight);

  // var gi = new GameInfo({
  //   name: "Balloon Pop",
  //   width: window.innerWidth,
  //   height: window.innerHeight,
  //   autoScale: true,
  //   themes: ["default"],
  //   gamesetsAllowed: true,  
  //   minimumGamesetCardsAllowed: 6,
  //   isTurnTaking: false,
  //   allowGameCardNavigation: true,
  // });  
  var gi = new GameInfo({
    name: "Balloon Pop",
    width: 1000,
    height: 600,
    autoScale: true,
    themes: ["default"],
    gamesetsAllowed: true,  
    minimumGamesetCardsAllowed: 6,
    isTurnTaking: false,
    allowGameCardNavigation: true,
  });
  sendToGameshell({
    eventType: "gameReady",
    message: gi
  });
};


var EndGameScene = new Phaser.Scene('EndGameScene');


EndGameScene.create = function () {
  console.log("EndGameScene====");
  //this.scene.start('SelectScene');
  var background = this.add.sprite(config.width/2, config.height* .35, 'logo').setScale(.5);

  this.add.text(500, config.height *.55, "Restart Game", { 
    font: "48px",
    wordWrap: { width: 520, useAdvancedWrap: true },
    color: '#46B5F4',
    align: 'center'
  }).setScale(.9).setAlpha(1).setOrigin(0.5, 0.5);

  this.restartBtn   = this.add.text(500, config.height *.8, "Restart Game", { 
    font: "38px",
    wordWrap: { width: 520, useAdvancedWrap: true },
    color: '#46B5F4',
    align: 'center'
  }).setScale(.9).setAlpha(1).setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    this.startGame();
  }.bind(this))
  .on('pointerover', function (pointer) { this.restartBtn.setScale(1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.restartBtn.setScale(.9); }.bind(this));
  

};

// ================================================================
var SelectScene = new Phaser.Scene('SelectScene');

SelectScene.preload = function () {
  self = this;

};

SelectScene.next = function () {
  this.scene.start('GameScene');
};

SelectScene.create = function () {


  this.background = this.add.image(config.width / 2, config.height / 2, 'background').setDisplaySize(config.width,config.height);
  var scale = this.background.scale;
  var scaleX = this.background.scaleX;
  var scaleY = this.background.scaleY;

  this.PickDialog = this.add.image(config.width / 2, config.height / 2, 'PickDialog').setScale(scaleX,scaleY);
  var closePos = this.PickDialog.getTopRight();
  this.CloseButton = this.add.image(closePos.x-20,closePos.y+20, 'CloseButton').setScale(scale*.9)
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Get Chips]");
    this.scene.start('GameScene', {balloonNo:1});
  }.bind(this))
  .on('pointerover', function (pointer) { this.CloseButton.setScale(scale*1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.CloseButton.setScale(scale*.9); }.bind(this));
  var width = config.width /6;
  var height = config.height /5;
  var i=1;
  for (var row = 0; row < 5; row++) {
    for(var col=0;col<4;col++){
      this.add.sprite(width + row*width,height+ col*height, 'balloon'+i).setOrigin(.5).setScale(scaleX*.35,scaleY*.35).setInteractive()
      .setData('balloon', i); 
      i++;
    }
  }
  this.input.on('gameobjectover', function (pointer, gameObject) {
    var balloonNo = gameObject.getData('balloon');
    if(balloonNo!=null){
      gameObject.setScale(scaleX*.4,scaleY*.4);
    }
  }, this);
  this.input.on('gameobjectout', function (pointer, gameObject) {
    var balloonNo = gameObject.getData('balloon');
    if(balloonNo!=null){
      gameObject.setScale(scaleX*.35,scaleY*.35);
    }
  }, this);

  this.input.on('gameobjectdown', function (pointer, gameObject) {
    var balloonNo = gameObject.getData('balloon');
    if(balloonNo!=null){
        // console.log("Selected Balloon="+balloonNo);
      this.scene.start('GameScene', {balloonNo:balloonNo});
      if (!currentPlayer.controlsEnabled ||!localPlayerIds.includes(currentPlayer.personId)){
        return;
      }
      else{
        sendToGameshell({
          eventType: "sendToAll",
          message: {
            type: "pickBalloon",
            data: balloonNo
          }
        });
      }

    }
  }, this);


};





var GameScene = new Phaser.Scene('GameScene');

GameScene.init = function (data) {
  console.log("Selected Balloon on init="+data.balloonNo);
  this.balloonNo = data.balloonNo;
};
GameScene.preload = function () {
  for (var i = 0; i < memoryCards.length; i++) {
    this.load.image('card'+memoryCards[i].id, memoryCards[i].frontImage);;
  }
};

GameScene.pumpBalloon = function () {
  this.clickCount++;
  if(this.clickCount<5){
    this.balloon.scale += .05;
  }else{
    this.clickCount=0;
    this.nextWord(true);
  }
};

GameScene.nextWord = function (withAnim){
  this.currentWord++;
  if(this.currentWord>=6)this.currentWord=0;
  this.card.setTexture('card'+memoryCards[this.currentWord].id);
  this.cardTitle.setText(memoryCards[this.currentWord].frontAlt);
  if(withAnim){
    this.NextButton.disableInteractive();
    this.tweens.add({
      targets: [this.balloon,this.String],
      ease: 'Sine.easeInOut',
      duration: 300,
      alpha:0,
      onCompleteParams: [this.balloon,this.String],
      onCompleteScope: this,
      onComplete: function(){
        this.balloon.setAlpha(1);
        this.balloon.setScale(this.scale*.7);
        this.String.setAlpha(1);
        this.balloon.y += 1000;
        this.String.y += 1000;
      }
    });
    this.tweens.add({
      targets: [this.balloon,this.String],
      ease: 'Sine.easeInOut',
      delay: 300,
      duration: 3000,
      y: "-=1000",
      onCompleteParams: [this.NextButton],
      onCompleteScope: this,
      onComplete: function(){
        this.NextButton.setInteractive();
      }
    });
  }
};

GameScene.create = function () {

 this.playerStats   = this.add.text(config.width / 2, config.height*.9, "Player Stats", { 
  font: "38px",
  wordWrap: { width: 520, useAdvancedWrap: true },
  color: '#ffffff',
  align: 'center'
}).setOrigin(.5).setAlpha(1);

 this.scene.bringToTop();

 this.background = this.add.image(config.width / 2, config.height / 2, 'background').setDisplaySize(1000,600);
 var scale = this.background.scale;
 var scaleX = this.background.scaleX;
 var scaleY = this.background.scaleY;

 this.Easel = this.add.image(config.width * .2, config.height * .6, 'Easel').setScale(scale*1);
 this.card = this.add.image(this.Easel.x, this.Easel.y - 80, 'card'+memoryCards[0].id).setScale(scale*.8);
 this.cardTitle   = this.add.text(this.Easel.x, this.Easel.y + 20, memoryCards[0].frontAlt, { 
  font: "38px",
  wordWrap: { width: 520, useAdvancedWrap: true },
  color: '#33121d',
  align: 'center'
}).setOrigin(.5).setAlpha(0);

 this.String = this.add.image(config.width * .65, config.height * .55 -5, 'String')
 .setOrigin(.5,0).setScale(scale*.7); 
 this.balloon = this.add.image(config.width * .65, config.height * .55, 'balloon'+this.balloonNo)
 .setOrigin(.5,1).setScale(scale*.7);

 this.balloonMove = this.tweens.add({
  targets: [this.balloon,this.String],
  ease: 'Sine.easeInOut',
  duration: 3000,
  x:"+=100",
  y: { value: "+=50", duration: 1500, ease: 'Bounce.easeOut',yoyo:true },
  yoyo:true,
  repeat:-1
});

 this.clickCount = 0;
 this.currentWord = 0;

 this.balloon.setInteractive({ cursor: 'url(assets/pointer2.cur), pointer' })
 .on('pointerdown', function () {
      // this.pumpBalloon();

  this.clickCount++;
  if(this.clickCount<5){
    this.balloon.scale += .05;
  }else{
    this.clickCount=0;
    this.nextWord(true);
  }

}.bind(this))
 .on('pointerover', function (pointer) { this.balloonMove.pause(); }.bind(this))
 .on('pointerout', function (pointer) { this.balloonMove.resume(); }.bind(this));

 this.NextButton = this.add.image(config.width *.2, config.height * .9, "NextButton").setScale(scale*.9)
 .setOrigin(0.5, 0.5).setInteractive()
 .on('pointerdown', function () {

  this.clickCount++;
  if(this.clickCount<5){
    this.balloon.scale += .05;
    this.nextWord(false);
  }else{
    this.clickCount=0;
    this.nextWord(true);
  }

}.bind(this))
 .on('pointerover', function (pointer) { this.NextButton.setScale(scale*1.1); }.bind(this))
 .on('pointerout', function (pointer) { this.NextButton.setScale(scale*.9); }.bind(this));

 this.isShown = true;

 this.ShowButton = this.add.image(config.width *.5, config.height * .9, "ShowButton").setScale(scale*.9)
 .setOrigin(0.5, 0.5).setInteractive()
 .on('pointerdown', function () {
  if(this.isShown) {
    this.ShowButton.setTexture("HideButton"); 
    this.cardTitle.setAlpha(1);
    this.isShown = false;
  }
  else{
    this.cardTitle.setAlpha(0);
    this.ShowButton.setTexture("ShowButton"); 
    this.isShown = true;
  }
}.bind(this))
 .on('pointerover', function (pointer) { this.ShowButton.setScale(scale*1.1); }.bind(this))
 .on('pointerout', function (pointer) { this.ShowButton.setScale(scale*.9); }.bind(this));

 this.PickButton = this.add.image(config.width *.8, config.height * .9, "PickButton").setScale(scale*.9)
 .setOrigin(0.5, 0.5).setInteractive()
 .on('pointerdown', function () {
  this.scene.start('SelectScene');
}.bind(this))
 .on('pointerover', function (pointer) { this.PickButton.setScale(scale*1.1); }.bind(this))
 .on('pointerout', function (pointer) { this.PickButton.setScale(scale*.9); }.bind(this));

 this.scale = scale;
};


var config = {
  type: Phaser.AUTO,
  parent: 'phaser-game',
  width: 1000,
  height: 600,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  backgroundColor: 0xffffff,
  scene: [ BootScene, Preloader , SelectScene, GameScene ,EndGameScene],
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
      gravity: { y: 0 }
    }
  },
};


var game = new Phaser.Game(config);




/**
 * Updates the players in the game
 */
 function updatePlayers() {
  if (!players || players.length == 0) return;

  var firstTime = true;
  var i=1;
  for (var playerId in players) {
    var player = players[playerId];
    console.log("updatePlayers , player="+player.name);
    if(self==null){
      console.log("self is null");
      return;
    }

    self.add.text(config.width *(.25 * i++), config.height *.1, player.name + " ("+playerId+"", { 
      font: "28px",
      wordWrap: { width: 520, useAdvancedWrap: true },
      color: '#46B5F4',
      align: 'center'
    }).setScale(.9).setAlpha(1).setOrigin(0.5, 0.5).setDepth(11);


    // updatePlayerControls(player);
    firstTime = false;
  }

  // updateCurrentPlayer(currentPlayer);
  // updateScores();
}

/**
 * Ends the game
 * Advances the game from the game area into the Ending screen
 */
 function endGame() {

  self.scene.start('EndGameScene');
}


// ===========================================================================
// HANDLONG GAMESHELL EVENTS
// ===========================================================================

/**
 * Starts the game, then send a message to all other game instances to start their games
 */
 function startGameHook() {
  // start/restart the game
  console.log("BELLOON_POP=== startGame(); at startGameHook"); 

  Preloader.startGame();

  // send message to all other games to start their game
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "startGame",
      data: { cardsOrder: cardsOrder, startingPlayerId: currentPlayer.personId }
    }
  });
}

/**
 * Sets Gameshell information (such as the current user type) in the game
 *
 * @param {*} gameshellInfo Information needed by the game about the Gameshell
 */
 function setGameshellInfoHook(gameshellInfo) {
  // handle the userType information
  userType = gameshellInfo.userType;
  console.log("BELLOON_POP=== UserType="+userType); 

  // hide the start/restart buttons if this was a player
  // 2 is Therapist UserType
  // if (userType != 1 && userType != 3) {
  //   $("#uiStartGame").show();
  //   $("#uiRestartGame").show();
  // } else {
  //   $("#uiStartGame").hide();
  //   $("#uiRestartGame").hide();
  // }

  // grab the user(s) playing this game on the current computer
  for (var player of gameshellInfo.players) {
    players[player.personId] = player;
    // these are the local players, grab their ids
    if (player.isLocal) {
      localPlayerIds.push(player.personId);
    }
  }

  currentPlayer = gameshellInfo.currentPlayer;
  
  console.log("BELLOON_POP=== updatePlayers();"); 
  updatePlayers();

  // the game is now ready
  isGameReady = true;
  handleMessageQueue();
}

/**
 * Changes the theme of the game based on the theme name passed to it
 *
 * @param {string} theme Name of theme to be applied.
 */
 function setThemeHook(theme) {
  // change the theme
  // setTheme(theme);
  console.log("BELLOON_POP=== setTheme(theme);"); 

  // since changing the theme will affect the cards
  // check if the game is already started
  if (isGameStarted()) {
    // then restart it again
    // startGame();
    console.log("BELLOON_POP=== startGame();"); 

  }

  // send message to all other games to change their themes
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "setTheme",
      data: { theme: theme, cardsOrder: cardsOrder }
    }
  });
}

/**
 * Changes the gameset for this game.
 *
 * @param {*} data Gameset to use in this game.
 */
 function setGamesetHook(data) {
  // update the gameset
  // setGameset(data);
  console.log("BELLOON_POP=== setGameset(data);"); 

  // since changing the gameset will affect the cards
  // check if the game is already started
  if (isGameStarted()) {
    // then restart it again
    // startGame();
    console.log("BELLOON_POP=== startGame();"); 
  }

  // send message to all other games to change their themes
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "setGameset",
      data: { gameset: data, cardsOrder: cardsOrder }
    }
  });
}

/**
 * Changes the current gameset card into a previous or next card.
 *
 * @param {string} direction The direction to change the gameset card. 'Next' to go forward or
 *                           'previous' to go backwards.
 */
 function setGamesetItemHook(direction) {
  // this method doesn't apply to this game
  // example implementation
  // grab the card
  //var card = direction == 'next' ? nextCard : previousCard;
  //setGamesetCard(card);
  // once the gameset card is changed, send a message to all other game instances
  //sendToGameshell({
  //  eventType: 'sendToAll',
  //  message: {
  //    type: 'setGamesetItem',
  //    data: {gamesetItem: card}
  //  }
  //});
 }

/**
 * Ends the game
 *
 * @param {*} data Data object sent by other game instances. Null if this game (or Gameshell) is initiating the event
 */
 function endGameHook() {
  // end the game
  // endGame();
  console.log("BELLOON_POP=== endGame();"); 

  self.scene.start('EndGameScene');
  // send message to all other games to start their game
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "endGame"
    }
  });
}

/**
 * Sets the players joined in this game
 *
 * @param {*} playersInfo List of player(s) [{id, name, controlsEnabled}]
 */
 function setPlayersHook(allPlayers) {
  // store new player ids
  var newPlayerIds = [];
  // loop over incoming players
  for (var player of allPlayers) {
    if (!players.hasOwnProperty(player.personId)) {
      // grab the list of ids of newly joined players
      newPlayerIds.push(player.personId);
    }
  }

  // add players to this game
  // setPlayers(allPlayers);
  console.log("BELLOON_POP=== setPlayers(allPlayers);"); 

  // send a message to all
  // informing them of the new players
  // and sending the gamestate to the new players
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "setPlayers",
      data: {
        // gameState: getGameState(),
        gameState: false,
        newPlayerIds: newPlayerIds,
        players: allPlayers
      }
    }
  });
}

/**
 * Sets the current selected player currently controls-enabled/allowed to play the game
 *
 * @param {*} player Player object {id, name, controlsEnabled}
 */
 function setCurrentPlayerHook(player) {
  // updateCurrentPlayer(player);
  console.log("BELLOON_POP=== updateCurrentPlayer(Players);"); 

  // inform other game instances
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "updateCurrentPlayer",
      data: player
    }
  });

  // inform Gameshell about player change
  sendToGameshell({
    eventType: "setCurrentPlayer",
    message: currentPlayer
  });
}

/**
 * Updates the controls of the player (by enabling/disabling them)
 *
 * @param {*} player Player object {id, name, controlsEnabled}
 */
 function updatePlayerControlsHook(player) {
  // updatePlayerControls(player);
  console.log("BELLOON_POP=== updatePlayerControls(Players);"); 

  // inform other game instances
  sendToGameshell({
    eventType: "sendToAll",
    message: {
      type: "updatePlayerControls",
      data: player
    }
  });
}





function handleGameMessageHook(message) {
  // if game is not ready yet
  if (!isGameReady) {
    // then store the message in a queue
    messageQueue.push(message);
    return;
  }

  var messageType = message.type;
  var data = message.data;
  switch (messageType) {
    /*
     * The following required methods enable the Gameshell to appropriately interact with the game.
     */
     case "startGame":
     Preloader.startGame(data);

     // startGame(data.cardsOrder, data.startingPlayerId);
     break;

     case "setTheme":
     setTheme(data.theme);
      // if the game had already started before updating the theme
     if (isGameStarted()) {
        // then restart the game
      startGame(data.cardsOrder, currentPlayer.personId);
    }
    break;

    case "setGameset":
    setGameset(data.gameset);
      // if the game had already started before updating the gameset
    if (isGameStarted()) {
        // then restart the game
      startGame(data.cardsOrder, currentPlayer.personId);
    }
    break;

    // not needed in this game
    //case 'setGamesetItem':
    //  setGamesetItem();
    //  break;

    case "endGame":
    endGame();
    break;

    case "setPlayers":
    var isLocalPlayerFound = false;
      // if the local player is one of the new players
    for (var playerId of data.newPlayerIds) {
      if (localPlayerIds.includes(playerId)) {
          // then set the game state
        setGameState(data.gameState);
        isLocalPlayerFound = true;
        break;
      }
    }

      // otherwise
    if (!isLocalPlayerFound) {
        // set the players
      setPlayers(data.players);
    }
    break;
    case "setLocalPlayers":
    setLocalPlayers(data);
    break;

    case "updateCurrentPlayer":
    updateCurrentPlayer(data);
    break;

    case "updatePlayerControls":
    updatePlayerControls(data);
    break;

    // case 'userJoined':
    //     userJoined(e.data);
    //     break;

    case "pauseGame":
    pauseGame(e.data);
    break;

    // case 'userLeft':
    //     userLeft(e.data);
    //     break;

    case "playersOnline":
    playersOnline(data);
    break;

    case "playersOffline":
    playersOffline(data);
    break;

    case "setGameState":
    setGameState(data);
    break;

    case "pickBalloon":
    if (message.loggedInPersonId === message.senderPersonId) return;
    self.scene.start('GameScene', {balloonNo:data});
    break;
  }
}

function handleMessageQueue() {
  for (var message of messageQueue) {
    handleGameMessageHook(message);
  }
  messageQueue = [];
}


// function bindEvent2(element, eventName, eventHandler) {
//   if (element.addEventListener) {
//     element.addEventListener(eventName, eventHandler, false);
//   } else if (element.attachEvent) {
//     element.attachEvent("on" + eventName, eventHandler);
//   }
// }


// function customfn(){
//   console.log("this is customfn outside of the scene");
// }


// setTimeout(function(){
//   console.log("New game");
//   // SelectScene.create();
//   GameScene.preload();

//   SelectScene.next();
//   // SelectScene.start('SelectScene');
// },3000);

