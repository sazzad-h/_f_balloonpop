var config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: window.innerWidth,
  height: window.innerHeight,
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH
  },
  scene: {
    preload: preload,
    create: create,
    update: update
  },
  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
      gravity: { y: 0 }
    }
  },
};

var game = new Phaser.Game(config);
var table = { width: 645, height: 312 };

function preload() {

  for (var i = 1; i < 20; i++) {
    this.load.image('balloon'+i, 'assets/images/Balloon'+i+'.png');;
  }
  this.load.image('background', 'assets/images/Background.png');
  this.load.image('CloseButton', 'assets/images/CloseButton.png');
  this.load.image('HideButton', 'assets/images/HideButton.png');
  this.load.image('NextButton', 'assets/images/NextButton.png');
  this.load.image('PickButton', 'assets/images/PickButton.png');
  this.load.image('ShowButton', 'assets/images/ShowButton.png');
  this.load.image('PickDialog', 'assets/images/PickDialog.png');
  this.load.image('String', 'assets/images/String.png');
  this.load.image('Easel', 'assets/images/Easel.png');

}



let memoryCards = [
  {
    id: 1,
    frontImage: "./assets/aurelia.svg",
    frontAlt: "Aurelia",
    order: 1,
    status: "Confirmed"
  },
  {
    id: 2,
    frontImage: "./assets/vue.svg",
    frontAlt: "Vue",
    order: 2,
    status: "Confirmed"
  },
  {
    id: 3,
    frontImage: "./assets/angular.svg",
    frontAlt: "Angular",
    order: 3,
    status: "Confirmed"
  },
  {
    id: 4,
    frontImage: "./assets/ember.svg",
    frontAlt: "Ember",
    order: 4,
    status: "Confirmed"
  },
  {
    id: 5,
    frontImage: "./assets/backbone.svg",
    frontAlt: "Backbone",
    order: 5,
    status: "Confirmed"
  },
  {
    id: 6,
    frontImage: "./assets/react.svg",
    frontAlt: "React",
    order: 6,
    status: "Confirmed"
  }
];

let currentMemoryCards = memoryCards.slice(0);
let isCustomGameSet = false;
let maxNumberOfUniqueCards = 6;
let cardsOrder = [];

// default variables
let currentThemeName = "default";
let currentTheme = themes["default"];
let lockBoard = false;
let firstCard, secondCard;
let matchesFound = 0;
// 1 is student userType
let userType = 1;

let players = {};
let playerScores = {};
let localPlayerIds = [];
let currentPlayer = null;

let isGameReady = false;
let messageQueue = [];

function create() {

  var gi = new GameInfo({
    name: "Balloon Pop",
    width: window.innerWidth,
    height: window.innerHeight,
    autoScale: true,
    themes: ["default"],
    gamesetsAllowed: true,  
    minimumGamesetCardsAllowed: 6,
    isTurnTaking: false,
    allowGameCardNavigation: true,
  });


  sendToGameshell({
    eventType: "gameReady",
    message: gi
  });

  this.background = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'background').setDisplaySize(window.innerWidth,window.innerHeight);
  var scale = this.background.scale;
  var scaleX = this.background.scaleX;
  var scaleY = this.background.scaleY;

  this.PickDialog = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'PickDialog').setScale(scaleX,scaleY);
  var closePos = this.PickDialog.getTopRight();
  this.CloseButton = this.add.image(closePos.x-20,closePos.y+20, 'CloseButton').setScale(scale*1);



  
  this.Easel = this.add.image(window.innerWidth * .2, window.innerHeight * .6, 'Easel').setScale(scale*1);
  this.balloon = this.add.image(window.innerWidth * .6, window.innerHeight * .35, 'balloon5').setScale(scale*.7);
  console.log("Background scale=",this.background.scale);
  console.log("Background scaleX=",this.background.scaleX);
  console.log("Background scaleY=",this.background.scaleY);


  this.NextButton = this.add.image(window.innerWidth *.2, window.innerHeight * .9, "NextButton").setScale(scale*.9)
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Get Chips]");
  }.bind(this))
  .on('pointerover', function (pointer) { this.NextButton.setScale(scale*1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.NextButton.setScale(scale*.9); }.bind(this));

  this.isShown = true;

  this.ShowButton = this.add.image(window.innerWidth *.5, window.innerHeight * .9, "ShowButton").setScale(scale*.9)
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    if(this.isShown) {
      this.ShowButton.setTexture("HideButton"); 
      this.isShown = false;
    }
    else{
      this.ShowButton.setTexture("ShowButton"); 
      this.isShown = true;
    }
  }.bind(this))
  .on('pointerover', function (pointer) { this.ShowButton.setScale(scale*1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.ShowButton.setScale(scale*.9); }.bind(this));

  this.PickButton = this.add.image(window.innerWidth *.8, window.innerHeight * .9, "PickButton").setScale(scale*.9)
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Get Chips]");
  }.bind(this))
  .on('pointerover', function (pointer) { this.PickButton.setScale(scale*1.1); }.bind(this))
  .on('pointerout', function (pointer) { this.PickButton.setScale(scale*.9); }.bind(this));

}

function create1() {
  this.table = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'table').setOrigin(0.5, 0.5);
  this.blackBG = this.add.image(window.innerWidth / 2, window.innerHeight / 2, 'blackBG').setDisplaySize(window.innerWidth,window.innerHeight).setDepth(9).setAlpha(0);

  this.cardPlace = [];
  this.cardPlace[0] = this.sound.add('cardPlace1', { volume: 1 });
  this.cardPlace[1] = this.sound.add('cardPlace2', { volume: 1 });
  this.cardPlace[2] = this.sound.add('cardPlace3', { volume: 1 });
  this.cardPlace[3] = this.sound.add('cardPlace4', { volume: 1 });
  this.cardSlide4 = this.sound.add('cardSlide4', { volume: 1 });
  this.applause = this.sound.add('applause', { volume: 1 });

  this.cards = ['2H','2H','4H','5H','6H'];
  this.deckCards = [];
  this.turn = 0;
  this.playerPos = [
  {x: window.innerWidth / 2, y: window.innerHeight * .35 },
  {x: window.innerWidth / 2 + 250, y: window.innerHeight /2 },
  {x: window.innerWidth / 2, y: window.innerHeight * .65 },
  {x: window.innerWidth / 2 - 250, y: window.innerHeight /2 },
  ]

  this.betChips = [];

  this.dealBtn = this.add.text(window.innerWidth / 2 - 225, window.innerHeight * .9, "[Deal Cards]", { fill: '#0f0' })
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Deal Cards]");
    this.player = this.add.image(window.innerWidth / 2, window.innerHeight * .2, 'player').setScale(.12).setDepth(10);
    for(var j=0;j<this.cards.length;j++){
     this.deckCards[j] = this.add.sprite(window.innerWidth / 2, window.innerHeight /2, "cards",this.cards[j])
     .setDepth(2).setScale(.25).setInteractive();
   }
   this.time.addEvent({ delay: 500, callback: popFromDeck ,callbackScope: this, repeat: 4 });
 }.bind(this))
  .on('pointerover', function (pointer) { this.setStyle({ fill: '#ff0' }); })
  .on('pointerout', function (pointer) { this.setStyle({ fill: '#0f0' }); });


  this.betChipBtn = this.add.text(window.innerWidth / 2 - 75, window.innerHeight * .9, "[Bet Chips]", { fill: '#0f0' })
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Bet Chips]");
    //args = { number of chips, radius, x,y of player or origin, self ref}
    betChip(5,100,this.player.x,this.player.y+75,this);
  }.bind(this))
  .on('pointerover', function (pointer) { this.setStyle({ fill: '#ff0' }); })
  .on('pointerout', function (pointer) { this.setStyle({ fill: '#0f0' }); });

  this.getChipBtn = this.add.text(window.innerWidth / 2 + 75, window.innerHeight * .9, "[Get Chips]", { fill: '#0f0' })
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Get Chips]");

    this.tweens.add({
      targets: this.betChips,
      ease: 'linear',
      duration: 500,
      x: this.player.x,
      y: this.player.y+75,
    });

  }.bind(this))
  .on('pointerover', function (pointer) { this.setStyle({ fill: '#ff0' }); })
  .on('pointerout', function (pointer) { this.setStyle({ fill: '#0f0' }); });

  this.winnerBtn = this.add.text(window.innerWidth / 2 + 225, window.innerHeight * .9, "[Winner]", { fill: '#0f0' })
  .setOrigin(0.5, 0.5).setInteractive()
  .on('pointerdown', function () {
    console.log("[Winner]");
    
    this.applause.play();
    var particles = this.add.particles('coin').setDepth(9);

    var manager =  particles.createEmitter({
      y: -50,
      x: { min: 0, max: window.innerWidth },
      lifespan: 2000,
      speedY: { min: 400, max: 700 },
      scale: { start: 0.4, end: 0.3 },
      quantity: 2,
      blendMode: 'ADD'
    });

    this.tweens.add({
      targets: this.player,
      ease: 'linear',
      duration: 1000,
      scale : .6,
      y: window.innerHeight / 2,
    });
    this.tweens.add({
      targets: this.blackBG,
      ease: 'linear',
      duration: 1000,
      alpha : .85,
    });
    this.tweens.add({
      targets: this.player,
      ease: 'linear',
      delay: 4000,
      duration: 1000,
      scale : .12,
      y: window.innerHeight * .2,
      onCompleteParams: [manager],
      onComplete: function(){
       manager.stop();
     }
   });
    this.tweens.add({
      targets: this.blackBG,
      ease: 'linear',
      delay: 4000,
      duration: 1000,
      alpha : 0,
    });

  }.bind(this))
  .on('pointerover', function (pointer) { this.setStyle({ fill: '#ff0' }); })
  .on('pointerout', function (pointer) { this.setStyle({ fill: '#0f0' }); });
  this.cardDepth = 2;
}

function  betChip(number,radius,x,y,self){ 
  // self.betChips = [];

  for(var j=0;j<number;j++){
    var chip = self.add.sprite(x, y, "coin").setScale(.25);
    var side = Phaser.Math.Between(0, 1);
    var oside = Phaser.Math.Between(0, radius);   
    self.betChips.push(chip);
    self.time.addEvent({ delay: 500*j, callback: moveChip , args:[chip,side,oside,radius] ,callbackScope: self });
  }
}
function moveChip(chip,full_radius,opposite,radius) {
//  var l_side = Phaser.Math.Between(0, 1);
//  if(full_radius){
//   if(l_side) var x=window.innerWidth/2 +radius,y=window.innerHeight/2 + opposite;
//   else  var x=window.innerWidth/2 - radius,y=window.innerHeight/2 + opposite;
// }
// else {
//  if(l_side) var y=window.innerHeight/2 + radius,x=window.innerWidth/2 +opposite;
//  else var y=window.innerHeight/2 + radius,x=window.innerWidth/2 -opposite; 
// }   

var x=Phaser.Math.Between(window.innerWidth/2 -200, window.innerWidth/2 +200); 
var y=Phaser.Math.Between(window.innerHeight/2 -100, window.innerHeight/2 +100);  
console.log("x,y="+x+","+y);  

this.cardSlide4.play();
this.tweens.add({
  targets: chip,
  ease: 'Quart.easeIn',
  duration: 500,
  x: x,
  y: y,
});
}

function popFromDeck(){
  console.log(" popFromDeck for "+this.turn);
  var card= this.deckCards.pop();
  card.setDepth(this.cardDepth++);
  var sound = Phaser.Math.Between(0, 3);
  this.cardPlace[0].play();

  this.tweens.add({
    targets: card,
    ease: 'linear',
    duration: 500,
    x: this.playerPos[this.turn].x,
    y: this.playerPos[this.turn].y,
  });
  this.turn++;
  if(this.turn>3) this.turn=0;


}

function update() { }
